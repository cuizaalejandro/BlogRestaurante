CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `city` (`id`, `nombre_ciudad`) VALUES
  (1, 'Pando'),
  (2, 'Cochabamba'),
  (3, 'La Paz'),
  (4, 'Santa Cruz'),
  (5, 'Oruro'),
  (6, 'Beni'),
  (7, 'Tarija'),
  (8, 'Sucre'),
  (9, 'Potosí');

CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estadope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `estado` (`id`, `estadope`) VALUES
  (1, 'PENDIENTE'),
  (2, 'ENVIADO'),
  (3, 'ENTREGADO');

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `role` (`role_id`, `role`) VALUES
  (1, 'ADMIN'),
  (2, 'LIMITED'),
  (3, 'ADMINREST');

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `identificador_opcion` bit(1) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_city` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FKk3xkd57m5tccde2nf2v0jgxvr` (`id_city`),
  CONSTRAINT `FKk3xkd57m5tccde2nf2v0jgxvr` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`user_id`, `active`, `email`, `identificador_opcion`, `last_name`, `name`, `password`, `id_city`) VALUES
  (1, b'1', 'alvaro@gmail.com', b'1', 'Cuiza', 'Alvaro', '$2a$10$pwgxOoSe.nx13syjhd5ySe5L50fOdCC7GjiIQz9/y8MjK2EdAMUQG', 1),
  (2, b'1', 'joseluis@gmail.com', b'1', 'Anagua', 'Jose Luis', '$2a$10$LWtMMjB2vDdHjTVywBw2KuYOiZDd9DhINh/BC2gg/6XGI7aH009h6', 2),
  (3, b'1', 'stanis@gmail.com', b'0', 'Vargas', 'Stanis', '$2a$10$zIG.xQTJr1Ogc/HL7Wbko.vS3rDVMawaJuoBDEcnG3HeXVlg.0Hee', 3),
  (4, b'1', 'robert@gmail.com', b'0', 'Camacho', 'Robert', '$2a$10$9FXorsnPhdKeQPSzu9tTSuP93jzPUroUwuMQwjRCUJWwmTGU3cuD6', 4),
  (5, b'1', 'daniel@gmail.com', b'0', 'Perez', 'Daniel', '$2a$10$Q1rnzrx49mJnryG9C9cE7u0LZVXshH5cidYnTRS8fGlcu.GHqvqT2', 5);

CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
  (1, 2),
  (2, 2),
  (3, 1),
  (4, 3),
  (5, 3);

CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `cell` int(11) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `email` varchar(20) NOT NULL,
  `latitud_restaurant` double DEFAULT NULL,
  `longitud_restaurant` double DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `phone` int(11) DEFAULT NULL,
  `id_city_rest` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpaourndks2r7m0w38nxptcrvn` (`id_city_rest`),
  KEY `FKsaxffw59e8gv87asawwx58hqp` (`user_id`),
  CONSTRAINT `FKpaourndks2r7m0w38nxptcrvn` FOREIGN KEY (`id_city_rest`) REFERENCES `city` (`id`),
  CONSTRAINT `FKsaxffw59e8gv87asawwx58hqp` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `restaurant` (`id`,  `address`, `cell`, `description`, `email`, `latitud_restaurant`, `longitud_restaurant`, `name`, `phone`, `id_city_rest`, `user_id`) VALUES
  (1, 'Pando y Av. América', 7345891, 'Los mejores pollos al espiedo', 'lucho@gmail.com', -11.094547205927595, -67.67699984582521, 'Lucho', 4748932, 1, 4),
  (2, 'Av. Circunvalación', 7289102, 'Silpancho de buen gusto', 'reysilpancho@gmail.c', -17.39353659348742, -66.16399222609216, 'Rey del silpancho', 4532125, 2, 5);

CREATE TABLE IF NOT EXISTS `choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `category_restaurant` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbumyvcpna0jfovtrcx9svtgpf` (`category_restaurant`),
  CONSTRAINT `FKbumyvcpna0jfovtrcx9svtgpf` FOREIGN KEY (`category_restaurant`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `choice` (`id`, `nombre`, `precio`, `category_restaurant`) VALUES
  (1, 'Combo escolar', 10, 1),
  (2, 'Combo economico', 14, 1),
  (3, 'Cuarto pollo', 20, 1),
  (4, 'Silpancho de carne', 17, 2),
  (5, 'Silpancho de pollo', 15, 2),
  (6, 'Doble intensidad', 20, 2);

CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud_pedido` double DEFAULT NULL,
  `longitud_pedido` double DEFAULT NULL,
  `nombredepedido` varchar(255) DEFAULT NULL,
  `preciopedido` int(11) DEFAULT NULL,
  `category_choice` int(11) DEFAULT NULL,
  `category_restaurant` int(11) DEFAULT NULL,
  `estadopedido` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdhwvqmq2jpbgpn4bmw6jsjg9h` (`category_choice`),
  KEY `FK1ccxg52nqt6snsqprcapdq6oa` (`category_restaurant`),
  KEY `FK61fy8f7da4975ue0b3ejh18hv` (`estadopedido`),
  KEY `FKrbqqge26x7tm1l5rp29qvvgwm` (`id_user`),
  CONSTRAINT `FK1ccxg52nqt6snsqprcapdq6oa` FOREIGN KEY (`category_restaurant`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK61fy8f7da4975ue0b3ejh18hv` FOREIGN KEY (`estadopedido`) REFERENCES `estado` (`id`),
  CONSTRAINT `FKdhwvqmq2jpbgpn4bmw6jsjg9h` FOREIGN KEY (`category_choice`) REFERENCES `choice` (`id`),
  CONSTRAINT `FKrbqqge26x7tm1l5rp29qvvgwm` FOREIGN KEY (`id_user`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pedido` (`id`, `latitud_pedido`, `longitud_pedido`, `nombredepedido`, `preciopedido`, `category_choice`, `category_restaurant`, `estadopedido`, `id_user`, `direccion`) VALUES
  (1, -11.106254468304305, -67.62547997506715, NULL, NULL, 2, 1, 1, 1, 'Av. América Nro. 1000'),
  (2, -17.394309579722034, -66.16297399249488, NULL, NULL, 6, 2, 1, 2, 'Gral. Galindo y Circunvalación');