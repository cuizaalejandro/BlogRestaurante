package com.example.BlogRestaurante.entities;

import javax.persistence.*;

@Entity
@Table(name = "estado")
public class Estado {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "estadope")
    private String estadope;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEstadope() {
        return estadope;
    }

    public void setEstadope(String estadope) {
        this.estadope = estadope;
    }


}